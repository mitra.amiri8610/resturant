# نرم افزار فروش غذا در رستوران
به کمک نرم افزار فروش غذا در رستوران مشتری میتواند به راحتی خرید و فروش خود را انجام بدهد.

# ویژگی های کلیدی نرم افزار
* جلوگیری از اتلاف وقت 
* صرفه جویی در هزینه
* افزایش رضایت مشتریان

# فازهای توسعه پروژه

* صفحه ی سفارش مشتری

* صفحه ی پرداخت صورتحساب

* صفحه ی منو

* صفحه ی مشتری

* صفحه ی ورود رمز 

* صفحه ی مدیریت رستوران

# تحلیل و طراحی پروژه 

در ابتدا به بررسی چند عملکرد اصلی نرم افزار در قالب سناریو فروش و پرداخت غذا در رستوران پرداختیم

* <a href="https://gitlab.com/namjoi/resturant/blob/master/documentation/SCENARIO.md">سناریو</a>

* <a href="https://gitlab.com/namjoi/resturant/blob/master/documentation/REQUIRMENTS.md">نیازمندی ها</a>


* <a href="https://gitlab.com/mitra.amiri8610/resturant/blob/master/README.md#ویژگی-های-کلیدی-نرم-افزار">ویژگی های کلیدی </a>

*  <a href="https://gitlab.com/mitra.amiri8610/resturant#فازهای-توسعه-پروژه">فاز های توسعه پروژه </a>


## توسعه دهندگان 

|نام و نام خانوادگی|   ID     |
|:---------------|:---------------|
|     zahra namjoo   |@namjoi |
|  maryam akbarian    | @akbarian1921 |  
|  mitra amiri   | @mitra.amiri8610 | 

